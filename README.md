# iospod

[![CI Status](https://img.shields.io/travis/yuanxc/iospod.svg?style=flat)](https://travis-ci.org/yuanxc/iospod)
[![Version](https://img.shields.io/cocoapods/v/iospod.svg?style=flat)](https://cocoapods.org/pods/iospod)
[![License](https://img.shields.io/cocoapods/l/iospod.svg?style=flat)](https://cocoapods.org/pods/iospod)
[![Platform](https://img.shields.io/cocoapods/p/iospod.svg?style=flat)](https://cocoapods.org/pods/iospod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

iospod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'iospod'
```

## Author

yuanxc, yuan.xucheng@yalla.live

## License

iospod is available under the MIT license. See the LICENSE file for more info.
