//
//  ViewController.swift
//  iospod
//
//  Created by yuanxc on 08/24/2021.
//  Copyright (c) 2021 yuanxc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var btn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .orange
        btn.setTitleColor(.white, for: .normal)
        btn.setTitle("按钮", for: .normal)
        btn.addTarget(self, action: #selector(btnClick(button:)), for: .touchUpInside)
        return btn
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(btn)
        btn.frame = CGRect(x: 100, y: 100, width: 100, height: 50)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func btnClick(button: UIButton) {
        button.isSelected = !button.isSelected
        button.backgroundColor = button.isSelected ? .red : .orange
    }

}

